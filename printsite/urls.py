from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'printsite.views.home', name='home'),
    # url(r'^printsite/', include('printsite.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    
    (r'^printsite/modify/(?P<job_id>[^/]+)/$', 'printjob.views.modify_page'),
    (r'^printsite/newjob/$', 'printjob.views.newjob_page'),
    (r'^printsite/save/$', 'printjob.views.save_page'),
    (r'^printsite/edit/(?P<job_id>[^/]+)/$', 'printjob.views.edit_page'),
    (r'^printsite/show/$', 'printjob.views.show_page'),
    (r'^printsite/create/$', 'printjob.views.create_page'),
    (r'^printsite/saveuser/$', 'printjob.views.saveuser_page'),
    (r'^printsite/login/$', 'printjob.views.login_view'),
    (r'^printsite/logout/$', 'printjob.views.logout_view'),
    (r'^printsite/$', 'printjob.views.home_page')
)
