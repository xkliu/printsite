# Create your views here.

from printjob.models import Job
from django.shortcuts import render_to_response, render
from django.http import HttpResponseRedirect
from django.db import DatabaseError
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from .forms import JobForm
from django.forms.models import model_to_dict
from time import time
from django.forms.models import inlineformset_factory

def home_page(request):
    return render(request, 'home.html', {})

def create_page(request):
    return render(request, 'create.html', {})
    
def modify_page(request, job_id):
    job = Job.objects.get(iden=job_id)
    if request.method == 'POST':
        form = JobForm(request.POST, request.FILES, instance=job)
        if form.is_valid():
            form.save()
    else:
        form = JobForm(instance=job)
    #JobInlineFormSet = inlineformset_factory(User, Job)
    #form = JobForm(instance=job)
    #form = JobInlineFormSet(request.POST, request.FILES, instance=job)
    return render(request, 'modify.html', {'form':form, 'job_id':job_id})

def newjob_page(request):
    if request.method == 'POST':
        form = JobForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
    else:
        form = JobForm()
    return render(request, 'newjob.html', {'form':form})

def show_page(request):
    user = request.user
    name = user.username
    try:
        user = request.user
        username = user.username
        prints = user.job_set.all()
    except User.DoesNotExist:
        return render_to_response("create.html")
    return render(request, 'show.html', {"name":username, "job":prints, "dates":int(str(time()).replace(".",""))})
    

def saveuser_page(request):
    name = request.GET["name"]
    email = request.GET["email"]
    password = request.GET["password"]
    newuser = User.objects.create_user(name, email, password)
    #newuser.save()
    return render(request, 'home.html')

def login_view(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(username=username, password=password)
    if user is not None:
        login(request, user)
        prints = user.job_set.all()
        
        #return render(request, 'show.html', {"name":username})
        return show_page(request)
    else:
        # Return an 'invalid login' error message.
        messages.error(request, 'invalid username or password')
        return render(request, 'home.html')

def logout_view(request):
    logout(request)
    return render(request, 'home.html')
    
def save_page(request):
    user = request.user
    job = Job()
    
    job.name = request.POST["name"]
    job.status = request.POST["status"]
    job.colour = request.POST["colour"]
    job.copies = request.POST["copies"]
    job.orientation = request.POST["orientation"]
    job.image = request.FILES["image"]
    job.iden = str(time()).replace(".","")
    user.job_set.add(job)
    
    job.save()
    prints = user.job_set.all()
    #return render(request, 'show.html', {"name":user.username, "job":prints})
    return show_page(request)

def edit_page(request, job_id):
    user = request.user
    
    job = Job.objects.get(iden=job_id)
    
    job.name = request.POST["name"]
    job.status = request.POST["status"]
    job.colour = request.POST["colour"]
    job.copies = request.POST["copies"]
    job.orientation = request.POST["orientation"]
    job.image = request.FILES["image"]
    
    job.save()
    prints = user.job_set.all()
    #return render(request, 'show.html', {"name":user.username, "job":prints})
    return show_page(request)
