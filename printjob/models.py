from django.db import models
from django import forms
from django.contrib.auth.models import User
from time import time

def get_upload_file_name(instance, filename):
    return "uploaded_files/%s_%s" % (str(time()).replace(".","_"), filename)

statuses = (('PENDING', 'pending'), ('CANCELED', 'canceled'), ('IN PROGRESS', 'in progress'))
orientations = (('Landscape', 'Landscape'), ('Portrait', 'Portrait'))
colours = (('Black/White', 'Black/White'), ('Colour', 'Colour'))


# Create your models here.
    
class Job(models.Model):
    name = models.CharField(max_length=30, primary_key=True)
    colour = models.CharField(max_length=30, choices=colours, blank=True)
    copies = models.IntegerField(blank=True)
    #image = models.ImageField(upload_to="/images/")
    image = models.FileField(upload_to=get_upload_file_name, blank=True)
    orientation = models.CharField(max_length=30, choices=orientations, blank=True)
    status = models.CharField(max_length=30, choices=statuses, blank=True)
    iden = models.CharField(max_length=30, blank=True)
    user = models.ForeignKey(User)