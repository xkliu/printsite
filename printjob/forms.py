from django import forms
from models import Job

statuses = ('pending', 'canceled')

class JobForm(forms.ModelForm):
    class Meta:
        model = Job
        #fields = ('name', 'status', 'image', 'copies', 'user')
        exclude = ('user', 'iden')
    
